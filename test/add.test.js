
import { expect } from 'chai';
import { Utils } from '../src/index.js'

describe('加法函数的测试', function () {
    it('1 加 1 应该等于 2', function () {
        expect(Utils.add(1, 1)).to.be.equal(2);
    });
});

describe('乘法函数的测试', function () {
    it('1 乘 1 应该等于 1', function () {
        expect(Utils.mult(1, 1)).to.be.equal(1);
    });
});